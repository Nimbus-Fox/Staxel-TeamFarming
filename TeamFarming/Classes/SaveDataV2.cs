﻿using System;
using System.Collections.Generic;

namespace NimbusFox.TeamFarming {
    [Serializable]
    public class SaveDataV2 : SaveData {
        public new Dictionary<Guid, TeamDataV2> Teams { get; set; }

        public SaveDataV2() {
            Version = "V2";
        }
    }
}
