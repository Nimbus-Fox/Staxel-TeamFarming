﻿using System;
using System.Linq;
using NimbusFox.TeamFarming.Classes;
using Plukit.Base;
using Staxel.Commands;
using Staxel.Logic;
using Staxel.Server;

namespace NimbusFox.TeamFarming.Staxel {
    class TeamFarmingOwnerCommand : ICommandBuilder {
        public string Execute(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            try {
                if (bits.Length > 1) {
                    switch (bits[1].ToLower()) {
                        case "help":
                            responseParams = new object[0];
                            return Help(bits.Skip(2).ToArray(), blob, connection, api, out responseParams);
                        case "disband":
                            responseParams = new object[0];
                            return Disband(bits.Skip(2).ToArray(), blob, connection, api, out responseParams);
                        case "rename":
                            responseParams = new object[0];
                            return Rename(bits.Skip(2).ToArray(), blob, connection, api, out responseParams);
                        case "invite":
                            responseParams = new object[0];
                            return Invite(bits.Skip(2).ToArray(), blob, connection, api, out responseParams);
                        case "togglepublic":
                            responseParams = new object[0];
                            return TogglePublic(bits.Skip(2).ToArray(), blob, connection, api, out responseParams);
                        case "kick":
                            responseParams = new object[0];
                            return Kick(bits.Skip(2).ToArray(), blob, connection, api, out responseParams);
                        case "ban":
                            responseParams = new object[0];
                            return Ban(bits.Skip(2).ToArray(), blob, connection, api, out responseParams);
                    }
                }
            } catch (Exception ex) {
                TeamManager.StoreException(ex, new { input = bits });
                responseParams = new object[3];
                responseParams[0] = "TeamFarming";
                responseParams[1] = "TeamFarming";
                responseParams[2] = "TeamFarming";
                return "mods.nimbusfox.exception.message";
            }

            responseParams = new object[0];
            return "mods.nimbusfox.teamfarming.owner.command.tf.description";
        }

        public string Kind => "tfowner";
        public string Usage => "mods.nimbusfox.teamfarming.owner.description";
        public bool Public => true;

        private static string Help(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[0];
            if (bits.Any()) {
                switch (bits[0].ToLower()) {
                    case "rename":
                        return "mods.nimbusfox.teamfarming.owner.command.rename.description";
                    case "disband":
                        return "mods.nimbusfox.teamfarming.owner.command.disband.description";
                    case "invite":
                        return "mods.nimbusfox.teamfarming.owner.command.invite.description";
                    case "kick":
                        return "mods.nimbusfox.teamfarming.owner.command.kick.description";
                    case "ban":
                        return "mods.nimbusfox.teamfarming.owner.command.ban.description";
                    case "togglepublic":
                        return "mods.nimbusfox.teamfarming.owner.command.togglepublic.description";
                }
            }

            return "mods.nimbusfox.teamfarming.owner.command.help.description";
        }

        private static string Disband(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[0];
            if (bits.Any()) {
                var teamName = bits[0];
                if (!TeamManager.UserInTeam(connection.Credentials.Uid)) {
                    return "mods.nimbusfox.teamfarming.error.notowner";
                }

                var team = TeamManager.GetUserTeam(connection.Credentials.Uid);

                if (team.OwnerUid != connection.Credentials.Uid) {
                    return "mods.nimbusfox.teamfarming.error.notowner";
                }

                if (team.Name != teamName) {
                    return "mods.nimbusfox.teamfarming.error.incorrectteamname";
                }

                responseParams = new object[3];
                responseParams[0] = connection.Credentials.Username;
                responseParams[1] = team.Name;
                responseParams[2] = team.GetDisbandMoney();

                foreach (var member in team.Members) {
                    api.MessagePlayer(member.Key, "mods.nimbusfox.teamfarming.message.teamdisbanded", responseParams);
                }

                TeamManager.Disband(team);

                responseParams = new object[1];
                responseParams[0] = team.Name;

                return "mods.nimbusfox.teamfarming.success.disbandteam";
            }

            return "mods.nimbusfox.teamfarming.owner.command.disband.description";
        }

        private static string Rename(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[0];
            if (bits.Any()) {
                string oldName;
                var output = TeamManager.RenameTeam(connection.Credentials.Uid, bits[0], out oldName);
                responseParams = new object[2];

                if (TeamManager.UserInTeam(connection.Credentials.Uid)) {
                    var team = TeamManager.GetUserTeam(connection.Credentials.Uid);
                    responseParams[0] = oldName;
                    responseParams[1] = team.Name;
                }

                return output;
            }

            return "mods.nimbusfox.teamfarming.owner.command.rename.description";
        }

        private static string Invite(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[0];

            if (bits.Any()) {
                var online = api.FetchPlayerNames();

                if (!online.Any(x => string.Equals(x, bits[0], StringComparison.CurrentCultureIgnoreCase))) {
                    responseParams = new object[1];
                    responseParams[0] = bits[0];
                    return "mods.nimbusfox.teamfarming.error.nomemberonline";
                }

                var target = online.First(x => string.Equals(x, bits[0], StringComparison.CurrentCultureIgnoreCase));
                var id = api.FindPlayerEntityId(target);
                Entity entity;

                if (!api.TryGetEntity(id, out entity)) {
                    responseParams = new object[1];
                    responseParams[0] = bits[0];
                    return "mods.nimbusfox.teamfarming.error.nomemberonline";
                }

                bool invited;
                var output = TeamManager.Invite(connection.Credentials.Uid, entity.PlayerEntityLogic.Uid(),
                    out invited);

                if (invited) {
                    responseParams = new object[1];
                    responseParams[0] = entity.PlayerEntityLogic.DisplayName();

                    var team = TeamManager.GetUserTeam(connection.Credentials.Uid);

                    foreach (var member in team.Members) {
                        api.MessagePlayer(member.Key, "mods.nimbusfox.teamfarming.message.newinvitetoteam",
                            new object[] {entity.PlayerEntityLogic.DisplayName(), team.Name, connection.Credentials.Username});
                    }

                    api.MessagePlayer(entity.PlayerEntityLogic.Uid(), "mods.nimbusfox.teamfarming.message.newinvite",
                        new object[] {team.Name, connection.Credentials.Username});
                }

                return output;
            }
            
            return "mods.nimbusfox.teamfarming.owner.command.invite.description";
        }

        private static string Kick(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[0];
            if (bits.Any()) {
                responseParams = new object[1];
                string target;
                LeaveResult result;
                var output = TeamManager.KickMember(connection.Credentials.Uid, bits[0], out result, out target);

                if (result == LeaveResult.Success) {
                    var team = TeamManager.GetUserTeam(connection.Credentials.Uid);
                    var online = api.FetchPlayerNames();
                    target = online.First(x => x == target);
                    var id = api.FindPlayerEntityId(target);
                    Entity entity;
                    if (api.TryGetEntity(id, out entity)) {
                        api.MessagePlayer(entity.PlayerEntityLogic.Uid(), "mods.nimbusfox.teamfarming.message.kicked",
                            new object[] {team.Name});
                    }

                    foreach (var member in team.Members) {
                        api.MessagePlayer(member.Key, "mods.nimbusfox.teamfarming.message.memberkicked", new object[] {
                            target, team.Name, team.GetTenth(), team.Petals
                        });
                    }
                }

                responseParams[0] = target;
                return output;
            }
            return "mods.nimbusfox.teamfarming.owner.command.kick.description";
        }

        private static string Ban(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[0];
            if (bits.Any()) {
                LeaveResult result;
                string targetname;
                bool member;

                var output = TeamManager.BanMember(connection.Credentials.Uid, bits[0], api, out result, out targetname,
                    out member);

                responseParams = new object[1];
                responseParams[0] = targetname;

                if (result == LeaveResult.Success) {
                    var team = TeamManager.GetUserTeam(connection.Credentials.Uid);

                    foreach (var teammember in team.Members) {
                        if (member) {
                            api.MessagePlayer(teammember.Key, "mods.nimbusfox.teamfarming.message.memberbanned",
                                new object[] { targetname, team.Name, team.GetTenth(), team.Petals });
                        } else {
                            api.MessagePlayer(teammember.Key, "mods.nimbusfox.teamfarming.message.ban",
                                new object[] {targetname, team.Name});
                        }
                    }

                    var id = api.FindPlayerEntityId(targetname);
                    Entity entity;
                    api.TryGetEntity(id, out entity);
                    api.MessagePlayer(entity.PlayerEntityLogic.Uid(), "mods.nimbusfox.teamfarming.message.banned",
                        new object[] {
                            team.Name, connection.Credentials.Username
                        });
                }

                return output;
            }

            return "mods.nimbusfox.teamfarming.owner.command.ban.description";
        }

        private static string TogglePublic(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[0];
            bool changed;
            var output = TeamManager.TogglePublic(connection.Credentials.Uid, out changed);

            if (!changed) {
                return output;
            }

            var team = TeamManager.GetUserTeam(connection.Credentials.Uid);

            foreach (var member in team.Members) {
                api.MessagePlayer(member.Key, output, new object[] {team.Name});
            }

            return "";
        }
    }
}
