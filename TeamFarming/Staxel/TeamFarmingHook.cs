﻿using System;
using System.Collections.Generic;
using System.Linq;
using Plukit.Base;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Modding;
using Staxel.Tiles;

namespace NimbusFox.TeamFarming.Staxel {
    public class TeamFarmingHook : IModHookV2 {

        public void Dispose() {
        }

        public void GameContextInitializeInit() {
            TeamManager.Init();
        }

        public void GameContextInitializeBefore() { }

        public void GameContextInitializeAfter() { }

        public void GameContextDeinitialize() { }

        public void GameContextReloadBefore() { }

        public void GameContextReloadAfter() { }

        public void UniverseUpdateBefore(Universe universe, Timestep step) {

            var players = new Lyst<Entity>();

            universe.GetPlayers(players);

            var teams = new List<TeamDataV2>();
            var petalDiff = new Dictionary<Guid, int>();
            var teamUpdates = new Dictionary<Guid, bool>();

            foreach (var player in players) {
                if (TeamManager.UserInTeam(player.PlayerEntityLogic.Uid())) {
                    var currentTeam = TeamManager.GetUserTeam(player.PlayerEntityLogic.Uid());
                    if (!teams.Contains(currentTeam)) {
                        teams.Add(currentTeam);
                        petalDiff.Add(currentTeam.TeamGuid, 0);
                        teamUpdates.Add(currentTeam.TeamGuid, false);

                        foreach (var kicked in new Dictionary<string, Validation>(currentTeam.Kicked.Where(x => !x.Value.Collected).ToDictionary())) {
                            if (!kicked.Value.Collected) {
                                petalDiff[currentTeam.TeamGuid] -= kicked.Value.Petals;
                                currentTeam.Kicked[kicked.Key] = new Validation {
                                    Petals = kicked.Value.Petals,
                                    Collected = true
                                };
                                teamUpdates[currentTeam.TeamGuid] = true;
                            }
                        }
                    }

                    if (currentTeam.Banned.Contains(player.PlayerEntityLogic.Uid()) &&
                        currentTeam.Members.ContainsKey(player.PlayerEntityLogic.Uid())) {
                        var current = currentTeam.Kicked[player.PlayerEntityLogic.Uid()];
                        currentTeam.Members.Remove(player.PlayerEntityLogic.Uid());
                        player.Inventory.ResetMoney(current.Petals);
                        currentTeam.Kicked.Remove(player.PlayerEntityLogic.Uid());
                        teamUpdates[currentTeam.TeamGuid] = true;
                        continue;
                    }

                    if (currentTeam.Kicked.ContainsKey(player.PlayerEntityLogic.Uid()) &&
                        currentTeam.Members.ContainsKey(player.PlayerEntityLogic.Uid())) {
                        var current = currentTeam.Kicked[player.PlayerEntityLogic.Uid()];
                        currentTeam.Members.Remove(player.PlayerEntityLogic.Uid());
                        player.Inventory.ResetMoney(current.Petals);
                        currentTeam.Kicked.Remove(player.PlayerEntityLogic.Uid());
                        teamUpdates[currentTeam.TeamGuid] = true;
                        continue;
                    }

                    if (currentTeam.Leaving.Contains(player.PlayerEntityLogic.Uid())) {
                        currentTeam.Leaving.Remove(player.PlayerEntityLogic.Uid());
                        currentTeam.Members.Remove(player.PlayerEntityLogic.Uid());
                        player.Inventory.ResetMoney(currentTeam.GetTenth());
                        petalDiff[currentTeam.TeamGuid] -= currentTeam.GetTenth();
                        teamUpdates[currentTeam.TeamGuid] = true;
                        continue;
                    }

                    if (currentTeam.Joining.Contains(player.PlayerEntityLogic.Uid())) {
                        currentTeam.Joining.Remove(player.PlayerEntityLogic.Uid());
                        teamUpdates[currentTeam.TeamGuid] = true;
                        petalDiff[currentTeam.TeamGuid] += player.Inventory.GetMoney();
                        continue;
                    }

                    if (currentTeam.Members[player.PlayerEntityLogic.Uid()] != player.PlayerEntityLogic.DisplayName()) {
                        currentTeam.Members[player.PlayerEntityLogic.Uid()] = player.PlayerEntityLogic.DisplayName();
                        teamUpdates[currentTeam.TeamGuid] = true;
                    }

                    if (currentTeam.OwnerUid == player.PlayerEntityLogic.Uid()) {
                        if (currentTeam.OwnerName != player.PlayerEntityLogic.DisplayName()) {
                            currentTeam.OwnerName = player.PlayerEntityLogic.DisplayName();
                            teamUpdates[currentTeam.TeamGuid] = true;
                        }
                    }

                    petalDiff[currentTeam.TeamGuid] += player.Inventory.GetMoney() - currentTeam.Petals;
                }

                var disbanded = TeamManager.GetDisbandedTeamsForUser(player.PlayerEntityLogic.Uid());

                if (disbanded.Any()) {
                    foreach (var team in disbanded) {
                        if (!teams.Contains(team)) {
                            teams.Add(team);
                            teamUpdates.Add(team.TeamGuid, false);
                        }

                        if (team.Kicked.ContainsKey(player.PlayerEntityLogic.Uid())) {
                            var temp = team.Kicked[player.PlayerEntityLogic.Uid()];
                            if (temp.Collected) {
                                player.Inventory.ResetMoney(temp.Petals);
                                teamUpdates[team.TeamGuid] = true;
                            }

                            continue;
                        }

                        player.Inventory.ResetMoney(team.GetDisbandMoney());
                        team.Collected.Add(player.PlayerEntityLogic.Uid());
                        teamUpdates[team.TeamGuid] = true;
                    }
                }
            }

            if (petalDiff.Any(x => x.Value != 0)) {
                foreach (var team in teams) {
                    if (petalDiff[team.TeamGuid] != 0) {
                        team.Petals += petalDiff[team.TeamGuid];
                        TeamManager.AddUpdateTeam(team);
                        teamUpdates[team.TeamGuid] = false;
                    }
                }

                foreach (var player in players) {
                    if (TeamManager.UserInTeam(player.PlayerEntityLogic.Uid())) {
                        var currentTeam = TeamManager.GetUserTeam(player.PlayerEntityLogic.Uid());
                        if (petalDiff[currentTeam.TeamGuid] != 0) {
                            player.Inventory.ResetMoney(currentTeam.Petals);
                        }
                    }
                }
            }

            if (teamUpdates.Any(x => x.Value)) {
                foreach (var team in teams) {
                    if (teamUpdates[team.TeamGuid]) {
                        TeamManager.AddUpdateTeam(team);
                    }
                }
            }
        }

        public void UniverseUpdateAfter() { }

        public bool CanPlaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanReplaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanRemoveTile(Entity entity, Vector3I location, TileAccessFlags accessFlags) {
            return true;
        }

        public void ClientContextInitializeInit() { }

        public void ClientContextInitializeBefore() { }

        public void ClientContextInitializeAfter() { }

        public void ClientContextDeinitialize() { }

        public void ClientContextReloadBefore() { }

        public void ClientContextReloadAfter() { }

        public void CleanupOldSession() { }
    }
}
