﻿using System;
using System.Collections.Generic;

namespace NimbusFox.TeamFarming {
    public class TeamDataV1 {
        public string Name { get; set; }
        public string OwnerName { get; set; }
        public string OwnerUid { get; set; }
        public Dictionary<string, string> Members { get; set; }
        public int Petals { get; set; }
        public bool Disbanded { get; set; }
        public List<string> Collected { get; set; }
        public Guid TeamGuid { get; set; }
        public bool Public { get; set; }
        public List<string> Invited { get; set; }
        public List<string> Banned { get; set; }
        public List<string> Joining { get; set; }
        public List<string> Leaving { get; set; }
        public Dictionary<string, Validation> Kicked { get; set; }

        public int GetTenth() {
            return (int) Math.Floor((decimal) (Petals / 10));
        }

        public int GetDisbandMoney() {
            return (int) Math.Floor((decimal) (Petals / Members.Count));
        }

        public TeamDataV1() {
            TeamGuid = Guid.NewGuid();
            Members = new Dictionary<string, string>();
            Collected = new List<string>();
            Invited = new List<string>();
            Banned = new List<string>();
            Joining = new List<string>();
            Leaving = new List<string>();
            Kicked = new Dictionary<string, Validation>();
        }
    }

    public class Validation {
        public int Petals { get; set; }
        public bool Collected { get; set; }
    }
}
