﻿using System;
using System.Collections.Generic;
using System.Linq;
using NimbusFox.FoxCore;
using NimbusFox.LandClaim.Interfaces;
using NimbusFox.TeamFarming.Classes;
using Plukit.Base;
using Staxel.Commands;
using Staxel.Logic;

namespace NimbusFox.TeamFarming {
    public static class TeamManager {
        private static SaveDataV2 _teams;
        private static SaveDataV2 _disbandedTeams;
        public static Fox_Core FoxCore;
        private const string TeamFile = "TF.db";
        private const string DisbandFile = "Disband.db";
        private static ILandClaim LandClaim;

        private static void FileSystemCheck() {
            var needFlush = false;

            if (!FoxCore.SaveDirectory.FileExists(TeamFile)) {
                _teams = new SaveDataV2 { Teams = new Dictionary<Guid, TeamDataV2>() };

                needFlush = true;
            } else {
                FoxCore.SaveDirectory.ReadFile<SaveDataV2>(TeamFile, saveData => { _teams = saveData; });
            }

            if (!FoxCore.SaveDirectory.FileExists(TeamFile)) {
                _disbandedTeams = new SaveDataV2 { Teams = new Dictionary<Guid, TeamDataV2>() };

                needFlush = true;
            } else {
                FoxCore.SaveDirectory.ReadFile<SaveDataV2>(DisbandFile, saveData => { _disbandedTeams = saveData; });
            }

            if (needFlush) {
                Flush();
            }
        }

        public static void Init() {
            FoxCore = new Fox_Core("Nimbusfox", "TeamFarming", "V0.3");
            LandClaim = Helpers.ResolveOptionalDependency<ILandClaim>("landclaim");
            FileSystemCheck();
        }

        private static void Flush() {
            foreach (var team in new Dictionary<Guid, TeamDataV2>(_teams.Teams.Where(x => x.Value.Disbanded).ToDictionary())) {
                _disbandedTeams.Teams.Add(team.Key, team.Value);
                _teams.Teams.Remove(team.Key);
            }

            foreach (var team in new Dictionary<Guid, TeamDataV2>(_disbandedTeams.Teams)) {
                if (team.Value.Collected.Count == team.Value.Members.Count + team.Value.Kicked.Count) {
                    _disbandedTeams.Teams.Remove(team.Key);
                }
            }

            FoxCore.SaveDirectory.WriteFile(TeamFile, _teams, false);
            FoxCore.SaveDirectory.WriteFile(DisbandFile, _disbandedTeams, false);
        }

        public static void AddUpdateTeam(TeamDataV2 team) {
            if (_disbandedTeams.Teams.ContainsKey(team.TeamGuid)) {
                _disbandedTeams.Teams[team.TeamGuid] = team;
            } else {
                if (_teams.Teams.ContainsKey(team.TeamGuid)) {
                    _teams.Teams[team.TeamGuid] = team;
                } else {
                    _teams.Teams.Add(team.TeamGuid, team);
                }
            }

            Flush();
        }

        public static TeamDataV2 GetTeam(string name) {
            var result = _teams.Teams.Where(x => string.Equals(x.Value.Name, name, StringComparison.CurrentCultureIgnoreCase)).ToList();

            return !result.Any() ? null : Enumerable.First(result).Value;
        }

        public static TeamDataV2 GetUserTeam(string userUid) {
            var result = _teams.Teams.Where(x => x.Value.Members.Any(y => y.Key == userUid)).ToList();

            return !result.Any() ? null : Enumerable.First(result).Value;
        }

        public static bool UserInTeam(string userUid) {
            return GetUserTeam(userUid) != null;
        }

        public static void Disband(TeamDataV2 data) {
            data.Disbanded = true;
            AddUpdateTeam(data);
        }

        public static LeaveResult RemoveUser(string userUid, out TeamDataV2 team, bool addToLeaving = true) {
            team = null;
            if (UserInTeam(userUid)) {
                team = GetUserTeam(userUid);
                if (team.OwnerUid == userUid) {
                    return LeaveResult.Error;
                }

                if (addToLeaving) {
                    team.Leaving.Add(userUid);
                } else {
                    team.Kicked.Add(userUid, new Validation {
                        Petals = team.GetTenth(),
                        Collected = false
                    });
                }
                AddUpdateTeam(team);
                return LeaveResult.Success;
            }

            return LeaveResult.Error;
        }

        public static string AddUser(string userUid, string playername, string name, out TeamDataV2 team) {
            team = null;
            if (!UserInTeam(userUid)) {
                team = GetTeam(name);

                if (team == null) {
                    return "mods.nimbusfox.teamfarming.error.teamnotexists";
                }

                if (team.Banned.Contains(userUid)) {
                    return "mods.nimbusfox.teamfarming.error.banned";
                }

                if (!team.Public) {
                    if (!team.Invited.Contains(userUid)) {
                        return "mods.nimbusfox.teamfarming.error.notinvited";
                    }
                }

                team.Members.Add(userUid, playername);
                team.Joining.Add(userUid);

                if (team.Invited.Contains(userUid)) {
                    team.Invited.Remove(userUid);
                }

                AddUpdateTeam(team);

                return "mods.nimbusfox.teamfarming.success.joinedteam";
            }
            return "mods.nimbusfox.teamfarming.error.inteam";
        }

        public static string CreateTeam(string userUid, string playername, string name, bool _public, out TeamDataV2 output) {
            if ((output = GetTeam(name)) != null) {
                return "mods.nimbusfox.teamfarming.error.teamexists";
            }

            if ((output = GetUserTeam(userUid)) != null) {
                return output.OwnerUid == userUid ? "mods.nimbusfox.teamfarming.error.ownerofteam" : "mods.nimbusfox.teamfarming.error.partofteam";
            }

            var newTeam = new TeamDataV2 {
                Members = new Dictionary<string, string> {
                    {userUid, playername }
                },
                OwnerUid = userUid,
                OwnerName = playername,
                Disbanded = false,
                Name = name,
                Public = _public
            };

            newTeam.Joining.Add(userUid);

            AddUpdateTeam(newTeam);
            output = newTeam;
            return "mods.nimbusfox.teamfarming.success.createteam";
        }

        public static List<TeamDataV2> GetDisbandedTeamsForUser(string userUid) {
            var output = new List<TeamDataV2>();

            foreach (var team in _disbandedTeams.Teams) {
                if (team.Value.Members.ContainsKey(userUid)) {
                    if (!team.Value.Collected.Contains(userUid)) {
                        output.Add(team.Value);
                    }
                }
            }

            return output;
        }

        public static string RenameTeam(string userUid, string newName, out string oldName) {
            oldName = "";
            if (!UserInTeam(userUid)) {
                return "mods.nimbusfox.teamfarming.error.noteam";
            }

            var team = GetUserTeam(userUid);

            if (team.OwnerUid != userUid) {
                return "mods.nimbusfox.teamfarming.error.notowner";
            }

            oldName = team.Name;
            team.Name = newName;
            AddUpdateTeam(team);

            return "mods.nimbusfox.teamfarming.success.teamrename";
        }

        public static string Invite(string userUid, string inviteUid, out bool invited) {
            invited = false;
            if (!UserInTeam(userUid)) {
                return "mods.nimbusfox.teamfarming.error.noteam";
            }

            var team = GetUserTeam(userUid);

            if (team.OwnerUid != userUid) {
                return "mods.nimbusfox.teamfarming.error.notowner";
            }

            team.Invited.Add(inviteUid);

            invited = true;

            return "mods.nimbusfox.teamfarming.success.invited";
        }

        public static string TogglePublic(string userUid, out bool changed) {
            changed = false;
            if (!UserInTeam(userUid)) {
                return "mods.nimbusfox.teamfarming.error.noteam";
            }

            var team = GetUserTeam(userUid);

            if (team.OwnerUid != userUid) {
                return "mods.nimbusfox.teamfarming.error.notowner";
            }

            team.Public = !team.Public;
            AddUpdateTeam(team);
            changed = true;
            return "mods.nimbusfox.teamfarming.message.toggle." + (team.Public ? "public" : "private");
        }

        public static string KickMember(string userUid, string target, out LeaveResult result, out string targetname) {
            result = LeaveResult.Error;
            targetname = target;
            if (!UserInTeam(userUid)) {
                return "mods.nimbusfox.teamfarming.error.noteam";
            }

            var team = GetUserTeam(userUid);

            if (team.OwnerUid != userUid) {
                return "mods.nimbusfox.teamfarming.error.notowner";
            }

            if (!team.Members.Values.Any(x => string.Equals(x, target, StringComparison.CurrentCultureIgnoreCase))) {
                return "mods.nimbusfox.teamfarming.error.nomember";
            }

            var member = team.Members.First(x =>
                string.Equals(x.Value, target, StringComparison.CurrentCultureIgnoreCase));

            if (member.Key == userUid) {
                return "mods.nimbusfox.teamfarming.error.ownerleaveteam";
            }

            targetname = member.Value;

            result = RemoveUser(member.Key, out _, false);

            return "mods.nimbusfox.teamfarming.success.kick";
        }

        public static string BanMember(string userUid, string target, ICommandsApi api, out LeaveResult result, out string targetname, out bool member) {
            result = LeaveResult.Error;
            targetname = target;
            member = false;
            if (!UserInTeam(userUid)) {
                return "mods.nimbusfox.teamfarming.error.noteam";
            }

            var team = GetUserTeam(userUid);

            if (team.OwnerUid != userUid) {
                return "mods.nimbusfox.teamfarming.error.notowner";
            }

            var online = api.FetchPlayerNames().ToList();

            if (!online.Any(x => string.Equals(x, target, StringComparison.CurrentCultureIgnoreCase))) {
                return "mods.nimbusfox.teamfarming.error.nomemberonline";
            }

            targetname = online.First(x => string.Equals(x, target, StringComparison.CurrentCultureIgnoreCase));

            var id = api.FindPlayerEntityId(targetname);
            Entity entity;
            if (!api.TryGetEntity(id, out entity)) {
                return "mods.nimbusfox.teamfarming.error.nomemberonline";
            }

            member = team.Members.Values.Any(x => string.Equals(x, target, StringComparison.CurrentCultureIgnoreCase));

            team.Banned.Add(entity.PlayerEntityLogic.Uid());
            team.Kicked.Add(entity.PlayerEntityLogic.Uid(), new Validation {
                Petals = team.GetTenth(),
                Collected = false
            });

            AddUpdateTeam(team);

            result = LeaveResult.Success;

            return "mods.nimbusfox.teamfarming.success.ban";
        }

        public static List<TeamDataV1> RankingsByMoney() {
            var output = new List<TeamDataV1>(_teams.Teams.Values);
            output.SortBy(x => x.Petals);
            return output;
        }

        public static void StoreException(Exception ex, object data) {
            FoxCore.ExceptionManager.HandleException(ex, new Dictionary<string, object> {
                {"data", data },
                {TeamFile, _teams },
                {DisbandFile, _disbandedTeams }
            });
        }

        public static Tuple<string, TeamDataV1> ForceRenameTeam(string teamName, string newName) {
            if (!_teams.Teams.Values.Any(x => string.Equals(x.Name, teamName, StringComparison.CurrentCultureIgnoreCase))) {
                return new Tuple<string, TeamDataV1>("mods.nimbusfox.teamfarming.error.admin.teamnotexists", null);
            }

            var team = _teams.Teams.Values.First(x =>
                string.Equals(x.Name, teamName, StringComparison.CurrentCultureIgnoreCase));
            
            team.Name = newName;
            AddUpdateTeam(team);

            return new Tuple<string, TeamDataV1>("mods.nimbusfox.teamfarming.success.admin.teamrename", team);
        }

        public static Tuple<string, TeamDataV1> ForceDisbandTeam(string teamName) {
            if (!_teams.Teams.Values.Any(x =>
                string.Equals(x.Name, teamName, StringComparison.CurrentCultureIgnoreCase))) {
                return new Tuple<string, TeamDataV1>("mods.nimbusfox.teamfarming.error.admin.teamnotexists", null);
            }

            var team = _teams.Teams.Values.First(x =>
                string.Equals(x.Name, teamName, StringComparison.CurrentCultureIgnoreCase));

            team.Disbanded = true;
            AddUpdateTeam(team);

            return new Tuple<string, TeamDataV1>("mods.nimbusfox.teamfarming.success.admin.disband", team);
        }
    }
}
