﻿using System;
using System.Collections.Generic;

namespace NimbusFox.TeamFarming {
    [Serializable]
    public class SaveData {
        public string Version { get; set; } = "v1";

        public Dictionary<Guid, TeamDataV1> Teams { get; set; }

        public SaveData() {
        }
    }
}
