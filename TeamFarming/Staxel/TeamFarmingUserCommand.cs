﻿using System;
using System.Linq;
using NimbusFox.TeamFarming.Classes;
using Plukit.Base;
using Staxel.Commands;
using Staxel.Server;

namespace NimbusFox.TeamFarming.Staxel {
    public class TeamFarmingUserCommand : ICommandBuilder {
        public string Execute(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {

            try {
                if (bits.Length > 1) {
                    switch (bits[1].ToLower()) {
                        case "help":
                            responseParams = new object[0];
                            return Help(bits.Skip(2).ToArray(), blob, connection, api, out responseParams);
                        case "create":
                            responseParams = new object[0];
                            return Create(bits.Skip(2).ToArray(), blob, connection, api, out responseParams);
                        case "status":
                            responseParams = new object[0];
                            return Status(bits.Skip(2).ToArray(), blob, connection, api, out responseParams);
                        case "join":
                            responseParams = new object[0];
                            return Join(bits.Skip(2).ToArray(), blob, connection, api, out responseParams);
                        case "leave":
                            responseParams = new object[0];
                            return Leave(bits.Skip(2).ToArray(), blob, connection, api, out responseParams);
                        case "ranks":
                            responseParams = new object[0];
                            return Ranking(bits.Skip(2).ToArray(), blob, connection, api, out responseParams);
                    }
                }
            } catch (Exception ex) {
                TeamManager.StoreException(ex, new { input = bits });
                responseParams = new object[3];
                responseParams[0] = "TeamFarming";
                responseParams[1] = "TeamFarming";
                responseParams[2] = "TeamFarming";
                return "mods.nimbusfox.exception.message";
            }

            responseParams = new object[0];
            return "mods.nimbusfox.teamfarming.command.tf.description";
        }

        public string Kind => "tf";
        public string Usage => "mods.nimbusfox.teamfarming.description";
        public bool Public => true;

        private static string Help(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[0];
            if (bits.Any()) {
                switch (bits[0].ToLower()) {
                    case "create":
                        return "mods.nimbusfox.teamfarming.command.create.description";
                    case "join":
                        return "mods.nimbusfox.teamfarming.command.join.description";
                    case "leave":
                        return "mods.nimbusfox.teamfarming.command.leave.description";
                    case "status":
                        return "mods.nimbusfox.teamfarming.command.status.description";
                    case "ranks":
                        return "mods.nimbusfox.teamfarming.command.ranks.description";
                }
            }

            return "mods.nimbusfox.teamfarming.command.help.description";
        }

        private static string Create(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            TeamDataV2 team;
            if (bits.Any()) {
                var _public = true;
                if (bits.Length > 1) {
                    if (!string.IsNullOrWhiteSpace(bits[1])) {
                        if (!bool.TryParse(bits[1], out _public)) {
                            responseParams = new object[0];
                            return "mods.nimbusfox.teamfarming.error.nonbooleancreate";
                        }
                    }
                }
                var text = TeamManager.CreateTeam(connection.Credentials.Uid, connection.Credentials.Username, bits[0], _public, out team);
                responseParams = new object[1];
                responseParams[0] = team.Name;
                return text;
            }
            responseParams = new object[0];
            return "mods.nimbusfox.teamfarming.command.create.description";
        }

        private static string Status(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[0];

            if (!TeamManager.UserInTeam(connection.Credentials.Uid)) {
                return "mods.nimbusfox.teamfarming.error.noteam";
            }

            var team = TeamManager.GetUserTeam(connection.Credentials.Uid);

            api.MessagePlayer(connection.Credentials.Uid, "mods.nimbusfox.teamfarming.message.status.1", new object[] { team.Name });
            api.MessagePlayer(connection.Credentials.Uid, "mods.nimbusfox.teamfarming.message.status.2",
                new object[] { team.OwnerName });
            api.MessagePlayer(connection.Credentials.Uid, "mods.nimbusfox.teamfarming.message.status.3",
                new object[] { team.Petals });
            api.MessagePlayer(connection.Credentials.Uid, "mods.nimbusfox.teamfarming.message.status.4." + (team.Public ? "public" : "private"),
                new object[] { });
            api.MessagePlayer(connection.Credentials.Uid, "mods.nimbusfox.teamfarming.message.status.5",
                new object[] { string.Join(", ", team.Members.Where(x => !team.Banned.Contains(x.Key) && !team.Kicked.ContainsKey(x.Key)).Select(x => x.Value)) });

            return "";
        }

        private static string Join(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[0];
            if (bits.Any()) {
                TeamDataV2 team;
                var output = TeamManager.AddUser(connection.Credentials.Uid, connection.Credentials.Username, bits[0],
                    out team);

                if (team != null) {
                    if (team.Members.ContainsKey(connection.Credentials.Uid)) {
                        responseParams = new object[1];
                        responseParams[0] = team.Name;

                        foreach (var member in team.Members) {
                            api.MessagePlayer(member.Key, "mods.nimbusfox.teamfarming.message.newmember",
                                new object[] { connection.Credentials.Username, team.Name });
                        }
                    }
                }

                return output;
            }

            return "mods.nimbusfox.teamfarming.command.join.description";
        }

        private static string Leave(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[0];
            TeamDataV2 team;
            var result = TeamManager.RemoveUser(connection.Credentials.Uid, out team);

            if (result == LeaveResult.Success) {
                responseParams = new object[1];
                responseParams[0] = team.Name;

                foreach (var member in team.Members) {
                    api.MessagePlayer(member.Key, "mods.nimbusfox.teamfarming.message.memberleaving",
                        new object[] { connection.Credentials.Username, team.Name, team.GetTenth(), team.Petals });
                }

                return "mods.nimbusfox.teamfarming.success.leftteam";
            }

            if (team != null) {
                if (team.OwnerUid == connection.Credentials.Uid) {
                    return "mods.nimbusfox.teamfarming.error.ownerleaveteam";
                }
            }
            return "mods.nimbusfox.teamfarming.error.noteam";
        }

        private static string Ranking(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[1];
            var rankings = TeamManager.RankingsByMoney();
            var output = "\n";

            if (!rankings.Any()) {
                responseParams = new object[0];
                return "mods.nimbusfox.teamfarming.message.rankings.noteams";
            }

            foreach (var team in rankings) {
                if (rankings.IndexOf(team) != 0) {
                    output += "\n";
                }

                output += $"{rankings.IndexOf(team) + 1}. {team.Name} ({team.Petals})";
            }

            responseParams[0] = output;
            return "mods.nimbusfox.teamfarming.message.rankings";
        }
    }
}
