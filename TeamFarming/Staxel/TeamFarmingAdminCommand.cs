﻿using System;
using System.Linq;
using Plukit.Base;
using Staxel.Commands;
using Staxel.Server;

namespace NimbusFox.TeamFarming.Staxel {
    public class TeamFarmingAdminCommand : ICommandBuilder {
        public string Execute(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {

            try {
                if (bits.Length > 1) {
                    switch (bits[1].ToLower()) {
                        case "help":
                            responseParams = new object[0];
                            return Help(bits.Skip(2).ToArray(), blob, connection, api, out responseParams);
                        case "rename":
                            responseParams = new object[0];
                            return Rename(bits.Skip(2).ToArray(), blob, connection, api, out responseParams);
                        case "disband":
                            responseParams = new object[0];
                            return Disband(bits.Skip(2).ToArray(), blob, connection, api, out responseParams);
                    }
                }
            } catch (Exception ex) {
                TeamManager.StoreException(ex, new { input = bits });
                responseParams = new object[3];
                responseParams[0] = "TeamFarming";
                responseParams[1] = "TeamFarming";
                responseParams[2] = "TeamFarming";
                return "mods.nimbusfox.exception.message";
            }

            responseParams = new object[0];
            return "mods.nimbusfox.teamfarming.admin.command.tf.description";
        }

        public string Kind => "tfadmin";
        public string Usage => "mods.nimbusfox.teamfarming.admin.description";
        public bool Public => false;

        private static string Help(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[0];
            if (bits.Any()) {
                switch (bits[0].ToLower()) {
                    case "rename":
                        return "mods.nimbusfox.teamfarming.admin.command.rename.description";
                    case "disband":
                        return "mods.nimbusfox.teamfarming.admin.command.disband.description";
                }
            }

            return "mods.nimbusfox.teamfarming.admin.command.help.description";
        }

        private static string Rename(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[0];
            if (bits.Length > 1) {
                var newName = bits[1];
                var oldname = bits[0];

                var output = TeamManager.ForceRenameTeam(oldname, newName);

                if (output.Item2 == null) {
                    return output.Item1;
                }

                foreach (var member in output.Item2.Members) {
                    api.MessagePlayer(member.Key, "mods.nimbusfox.teamfarming.message.admin.teamrename",
                        new object[] {newName});
                }

                responseParams = new object[2];
                responseParams[0] = oldname;
                responseParams[1] = newName;
                return output.Item1;
            }

            return "mods.nimbusfox.teamfarming.admin.command.rename.description";
        }

        private static string Disband(string[] bits, Blob blob, ClientServerConnection connection, ICommandsApi api,
            out object[] responseParams) {
            responseParams = new object[0];
            if (bits.Any()) {
                var output = TeamManager.ForceDisbandTeam(bits[0]);

                if (output.Item2 == null) {
                    return output.Item1;
                }

                foreach (var member in output.Item2.Members) {
                    api.MessagePlayer(member.Key, "mods.nimbusfox.teamfarming.message.admin.disband", new object[] {
                        connection.Credentials.Username, output.Item2.Name, output.Item2.GetDisbandMoney()
                    });
                }

                responseParams = new object[1];
                responseParams[0] = output.Item2.Name;
                return output.Item1;
            }
            return "mods.nimbusfox.teamfarming.admin.command.disband.description";
        }
    }
}
